﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Settings;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Mongo
{
    public class MongoDatabaseSettings : IMongoDatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }




}
