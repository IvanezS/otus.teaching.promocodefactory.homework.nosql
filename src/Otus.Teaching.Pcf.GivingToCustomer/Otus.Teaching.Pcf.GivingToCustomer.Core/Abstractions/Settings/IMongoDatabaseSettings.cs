﻿namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Settings
{
    public interface IMongoDatabaseSettings
    {
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
