﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Settings;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _db;
        private readonly IMongoDatabaseSettings _settings;
        public MongoDbInitializer(IMongoDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            _db = client.GetDatabase(settings.DatabaseName);
            _settings = settings;
        }

        public void InitializeDb()
        {

            _db.DropCollection("Preferences");
            _db.DropCollection("Customers");

            _db.CreateCollection("Preferences");
            _db.CreateCollection("Customers");

            _db.GetCollection<Preference>("Preferences").InsertMany(FakeDataFactory.Preferences);
            _db.GetCollection<Customer>("Customers").InsertMany(FakeDataFactory.Customers);
        }

    }
}