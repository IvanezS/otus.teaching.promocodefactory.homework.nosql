﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Settings;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class MongoRep<T> : IRepository<T>
        where T : BaseEntity
    {


        private readonly IMongoCollection<T> _db;

        public MongoRep(IMongoDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _db = database.GetCollection<T>(typeof(T).Name);
        }


        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await _db.FindAsync(a => true);

            return entities.ToList();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await _db.FindAsync(x => x.Id == id);

            return entity.Single();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await _db.FindAsync(x => ids.Contains(x.Id));
            return entities.ToList();
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            var entity = await _db.FindAsync(predicate);
            return await entity.FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            var entity = await _db.FindAsync(predicate);
            return await entity.ToListAsync();
        }

        public async Task AddAsync(T entity)
        {
            await _db.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            await _db.ReplaceOneAsync(a => a.Id == entity.Id, entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await _db.DeleteOneAsync(a => a.Id == entity.Id);
        }

    }
}
