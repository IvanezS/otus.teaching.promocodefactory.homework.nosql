﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Settings;


namespace Otus.Teaching.Pcf.Administration.WebHost.Models.Mongo
{
    public class MongoAdminDatabaseSettings : IMongoDatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }




}
