﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Settings;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoAdminRep<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private readonly IMongoCollection<T> _db;

        public MongoAdminRep(IMongoDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _db = database.GetCollection<T>(typeof(T).Name);
        }
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var result = await _db.FindAsync(employee => true);
            return result.ToList();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var result = await _db.FindAsync(employee => employee.Id == id);
            return result.Single();
        }

        public async Task AddAsync(T empl)
        {
            await _db.InsertOneAsync(empl);
        }

        public async Task UpdateAsync(T empl)
        {
            await _db.ReplaceOneAsync(employee => employee.Id == empl.Id, empl);
        }

        public async Task DeleteAsync(T empl)
        {
            await _db.DeleteOneAsync(employee => employee.Id == empl.Id);
        }

        public Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

    }
}
