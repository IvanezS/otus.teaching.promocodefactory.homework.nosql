﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Settings;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _db;
        private readonly IMongoDatabaseSettings _settings;
        public MongoDbInitializer(IMongoDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            _db = client.GetDatabase(settings.DatabaseName);
            _settings = settings;
        }

        public void InitializeDb()
        {

            _db.DropCollection("Employee");
            _db.DropCollection("Role");

            _db.CreateCollection("Employee");
            _db.CreateCollection("Role");

            _db.GetCollection<Employee>("Employee").InsertMany(FakeDataFactory.Employees);
            _db.GetCollection<Role>("Role").InsertMany(FakeDataFactory.Roles);
        }

    }
}