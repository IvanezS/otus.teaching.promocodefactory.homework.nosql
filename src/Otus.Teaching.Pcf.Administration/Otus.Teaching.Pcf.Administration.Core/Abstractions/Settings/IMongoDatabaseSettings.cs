﻿namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Settings
{
    public interface IMongoDatabaseSettings
    {
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
